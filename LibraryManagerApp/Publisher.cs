using System;

namespace LibraryManager
{
    public class Publisher
    {
        public string Name { get; set; }

        public Publisher(string name)
        {
            Name = name;
        }

		public override string ToString()
		{
			return $"{Name}";
		}

		public static Publisher GetFromInput()
		{
			var name = InputUtils.GetStringInput("Publisher name: ");
			return new Publisher(name);
		}
    }
}