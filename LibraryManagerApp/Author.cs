using System;
using System.Data.SqlClient;

namespace LibraryManager
{
    public class Author
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public Author(string firstName, string lastName, DateTime birthDate)
        {
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
        }

		public bool AddToDatabase(SqlConnection connection)
		{
			var query = "INSERT INTO Authors(FirstName, LastName, BirthDate) " +
						"VALUES (@FirstName, @LastName, @BirthDate)";
			try
			{
				var command = new SqlCommand(query);
				command.Connection = connection;
				command.Parameters.AddWithValue("FirstName", FirstName);
				command.Parameters.AddWithValue("LastName", LastName);
				command.Parameters.AddWithValue("BirthDate", BirthDate);
				var rows = command.ExecuteNonQuery();
				Console.WriteLine($"{rows} rows affected");
				if (rows > 0) return true;
				return false;
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				return false;
			}
		}

		public override string ToString()
		{
			return $"{FirstName} {LastName}, Born {BirthDate.ToShortDateString()}";
		}

		public static Author GetFromInput()
		{
			var firstName = InputUtils.GetStringInput("Author first name: ");
			var lastName = InputUtils.GetStringInput("Author last name: ");
			var birthDate = InputUtils.GetDateTimeInput("Date of birth");

			return new Author(firstName, lastName, birthDate);
		}
    }
}