using System;

namespace LibraryManager
{
    public enum GenreType : byte
    {
        Action,
        Romance,
        Adventure,
        Mystery,
        Fantasy,
    }

    public class Genre
    {
        public GenreType Type { get; set; }

        public Genre(GenreType type)
        {
            Type = type;
        }

		public override string ToString()
		{
			return $"{Type.ToString()}";
		}

		public static Genre GetFromInput()
		{
			var genres = Enum.GetValues(typeof(GenreType));
			var message = string.Empty;
			foreach(var genre in genres)
			{
				message += $"({(byte)genre}) {genre.ToString()}\n";
			}
			message += "Genre Id: ";
			var type = (GenreType)InputUtils.GetNumberInput(1, Enum.GetValues(typeof(GenreType)).Length-1, message);
			return new Genre(type);
		}
    }
}