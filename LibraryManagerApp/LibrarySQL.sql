CREATE DATABASE AccentureLibrary

USE AccentureLibrary

CREATE TABLE Author
(
	Id int primary key identity(1,1),
	FirstName varchar(30) not null,
	LastName varchar(30) not null,
	BirthDate DateTime not null,
	Nationality varchar(20) not null,
	CONSTRAINT UQ_FULLNAME UNIQUE(FirstName, LastName)
)

CREATE TABLE Publisher
(
	Id int primary key identity(1,1),
	Name varchar(30) unique not null
)

CREATE TABLE Genre
(
	Id int primary key identity(1,1),
	Name varchar(30) unique not null
)

CREATE TABLE Book
(
	Id int primary key identity(1,1),
	ISBN varchar(20) unique not null,
	Title varchar(50) not null,
	Id_Publisher int foreign key references Publisher(Id) not null,
	Id_Genre int foreign key references Genre(Id) not null,
	Edition int not null,
	PublishDate DateTime not null,
	Url_Cover varchar(200),
	PageCount int not null,
	CONSTRAINT UQ_INFO UNIQUE(Title, Id_Author, Id_Publisher, Edition),
	CONSTRAINT P_PAGECOUNT CHECK(PageCount>0),
	CONSTRAINT P_EDITION CHECK(Edition>0),
)

CREATE TABLE Book_Author
(
	Id_Book int foreign key references Book(Id) not null,
	Id_Author int foreign key references Author(Id) not null,
	CONSTRAINT UQ_PAIR UNIQUE(Id_Book, Id_Author)
)