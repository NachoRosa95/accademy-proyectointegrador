using System;

namespace LibraryManager
{
    public class Book
    {
        public string Title { get; set; }
		public string ISBN { get; set; }
        public Author Author { get; set; }
        public Genre Genre { get; set; }
        public Publisher Publisher { get; set; }
		public DateTime PublishDate { get; set; }

        public Book(string title, string isbn, Author author, Genre genre, Publisher publisher, DateTime publishDate)
        {
            Title = title;
			ISBN = isbn;
            Author = author;
            Genre = genre;
            Publisher = publisher;
			PublishDate = publishDate;
        }

		public override string ToString()
		{
			return $"\nTitle: {Title}\nISBN: {ISBN}\nAuthor: {Author.ToString()}\nGenre: {Genre.ToString()}\nPublisher: {Publisher.ToString()}\nPublish Date: {PublishDate.ToShortDateString()}";
		}

		public static Book GetFromInput()
		{
			Console.Write("Title: ");
			var title = Console.ReadLine();
			var isbn = InputUtils.GetStringInput("ISBN: ");
			var author = Author.GetFromInput();
			var genre = Genre.GetFromInput();
			var publisher = Publisher.GetFromInput();
			var publishDate = InputUtils.GetDateTimeInput("Publish date");
			return new Book(title, isbn, author, genre, publisher, publishDate);
		}
    }
}