using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryManager
{
    public class Program
    {
        static void Main(string[] args)
        {
            var newBook = Book.GetFromInput();
            Console.WriteLine(newBook.ToString());
            Console.ReadLine();
        }
    }
}