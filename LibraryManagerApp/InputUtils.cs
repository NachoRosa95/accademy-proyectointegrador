﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager
{
	class InputUtils
	{
		public static DateTime GetDateTimeInput(string message = "Insert date: ")
		{
			Console.WriteLine(message);
			var year = (int)GetNumberInput(1800, 2020, "Year: ");
			var month = (int)GetNumberInput(1, 12, "Month: ");
			var day = (int)GetNumberInput(1, 31, "Day: ");
			return new DateTime(year, month, day);
		}

		public static float GetNumberInput(float min = 0, float max = 100, string message = "Insert number: ")
		{
			var output = float.MinValue;
			if (min > max) return output;
			var exceptionHappened = false;
			do
			{
				Console.Write(message);
				try
				{
					output = float.Parse(Console.ReadLine());
					exceptionHappened = false;
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
					exceptionHappened = true;
				}
			} while (output < min || output > max || exceptionHappened);
			return output;
		}

		public static string GetStringInput(string message = "Insert string: ")
		{
				Console.Write(message);
				return Console.ReadLine();
		}

		public static string ToPaddedString(List<object> inputs, int padAmount = 16)
		{
			var formattedString = string.Empty;
			foreach (var input in inputs)
			{
				formattedString += input.ToString().PadRight(padAmount);
			}
			return formattedString;
		}
	}
}
